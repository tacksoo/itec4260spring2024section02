package com.company;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URL;

public class SteamAppStore {

    public static String STEAM_URL = "https://store.steampowered.com/api/appdetails?appids=";
    // this suffix will make sure you only get US based results
    public static String STEAM_SUFFIX = "&filters=&cc=US&l=english";


    public static Game getGameFromSteam(String appID) {
        Game g = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            URL url = new URL(STEAM_URL + appID + STEAM_SUFFIX);
            JsonNode root = mapper.readTree(url);
            //name
            String name = root.get(appID).get("data").get("name").asText();
            String releasedDate = root.get(appID).get("data").get("release_date").get("date").asText();
            //releaseDate
            int retailPrice = root.get(appID).get("data").get("price_overview").get("initial").asInt();
            //genres
            JsonNode genres = root.get(appID).get("data").get("genres");
            // genre: grab the first genre only
            String genre = genres.get(0).get("description").asText();
            // platform are set to windows, mac, linux with boolean (true or false)
            boolean windows = root.get(appID).get("data").get("platforms").get("windows").asBoolean();
            boolean mac = root.get(appID).get("data").get("platforms").get("mac").asBoolean();
            boolean linux = root.get(appID).get("data").get("platforms").get("linux").asBoolean();
            String platform = "";
            if (windows) {
                platform += "Windows,";
            }
            if (mac) {
                platform += "Mac,";
            }
            if (linux) {
                platform += "Linux";
            }
            if (platform.endsWith(",")) {
                platform = platform.substring(0, platform.length()-1);
            }
            JsonNode devs = root.get(appID).get("data").get("developers");
            String developer = devs.get(0).asText();
            int recCount = root.get(appID).get("data").get("recommendations").get("total").asInt();
            g = new Game(appID,platform, name, releasedDate, developer, genre, retailPrice, recCount);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return g;
    }

}
