package com.company;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Store {

    private Inventory inventory;

    public Store(Inventory inv) {
        inventory = inv;
    }

    public void loadInventoryFromWeb(String csvAddress) {
        //create URL object
        try {
            URL url = new URL(csvAddress);
            //use IOUtils.toString() to read the csv file
            String data = IOUtils.toString(url,"UTF-8");
            //game object based on each line (helper method)
            CSVParser parser = new CSVParser(new StringReader(data), CSVFormat.DEFAULT.builder().setHeader().build());
            for(CSVRecord record: parser) {
                Game g = getGameFromRecord(record);
                inventory.add(g);
            }
            //populate inventory with it
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Game getGameFromRecord(CSVRecord record) {
        //appID,platform,name,releaseDate,developer,genre,retailPrice,recommendationCount
        String appID = record.get("appID");
        String platform = record.get("platform");
        String name = record.get("name");
        String releaseDate = record.get("releaseDate");
        String developer = record.get("developer");
        String genre = record.get("genre");
        int retailPrice = Integer.parseInt(record.get("retailPrice"));
        int recCount = Integer.parseInt(record.get("recommendationCount"));
        Game g = new Game(appID, platform, name, releaseDate, developer, genre, retailPrice, recCount);
        return g;
    }

}
