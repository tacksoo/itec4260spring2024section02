package com.company;

import java.util.ArrayList;
import java.util.List;

public class Inventory {

    private List<Game> games = new ArrayList<>();

    public Inventory() {

    }

    public int getSize() {
        return games.size();
    }

    public void add(Game g) {
        for (int i = 0; i < games.size(); i++) {
            // check for duplicate game
            if (games.get(i).getAppID().equals(g.getAppID())) {
                games.set(i, g);
                return;
            }
        }
        games.add(g); // no duplicate found
    }

    public void remove(Game g) {
        for (int i = 0; i < games.size(); i++) {
            if (games.get(i).getAppID().equals(g.getAppID())) {
                games.remove(i);
            }
        }
    }

    public void printAveragePriceOfAllGames() {
        double total = 0;
        for (int i = 0; i < games.size(); i++) {
            total += games.get(i).getRetailPrice();
        }
        System.out.println(total/games.size());
    }

    public Game findCheapestGame() {
        if (games.size() == 0) return null;
        Game cheapest = games.get(0);
        for (int i = 1; i < games.size(); i++) {
            if (cheapest.getRetailPrice() > games.get(i).getRetailPrice()) {
                cheapest = games.get(i);
            }
        }
        return cheapest;
    }

    public Game findMostHighlyRatedGame() {
        if (games.size() == 0) return null;
        Game highest = games.get(0);
        for (int i = 1; i < games.size(); i++) {
            if (highest.getRecommendationCount() < games.get(i).getRecommendationCount()) {
                highest = games.get(i);
            }
        }
        return highest;
    }
}
