package com.company;

import java.util.Objects;

public class Game {

    private String appID;
    private String platform;
    private String name;
    private String releaseDate;
    private String developer;
    private String genre;
    private int retailPrice;
    private int recommendationCount;

    public Game() {
    }

    public Game(String appID, String platform, String name, String releaseDate, String developer, String genre, int retailPrice, int recommendationCount) {
        this.appID = appID;
        this.platform = platform;
        this.name = name;
        this.releaseDate = releaseDate;
        this.developer = developer;
        this.genre = genre;
        this.retailPrice = retailPrice;
        this.recommendationCount = recommendationCount;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(int retailPrice) {
        this.retailPrice = retailPrice;
    }

    public int getRecommendationCount() {
        return recommendationCount;
    }

    public void setRecommendationCount(int recommendationCount) {
        this.recommendationCount = recommendationCount;
    }

    public void printGame() {
        System.out.println(appID);
        System.out.println(platform);
        System.out.println(name);
        System.out.println(releaseDate);
        System.out.println(developer);
        System.out.println(genre);
        System.out.println(retailPrice);
        System.out.println(recommendationCount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return appID.equals(game.appID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(appID);
    }
}
