import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.sql.*;
import java.time.Duration;
import java.util.Date;
import java.util.List;

public class DatabaseTest {

    private static Connection connection;
    public static String DB_URL = "jdbc:sqlite:news.sqlite";
    public static int COUNT_NEWS_ON_FRONTPAGE = 30;
    public static WebDriver driver;

    @BeforeClass
    public static void setUp() throws Exception {
        connection = DriverManager.getConnection(DB_URL);
        driver = new FirefoxDriver();
    }

    @Test
    @Ignore
    public void addRowToHeadlines() throws Exception {
        String sql = "insert into headlines values (null, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, "world peace finally achieved! 2024!");
        ps.setString(2, "https://www.cnn.com");
        ps.executeUpdate();
    }

    @Test
    public void getHackernews() {
        driver.get("https://news.ycombinator.com");
        List<WebElement> spans = driver.findElements(By.className("titleline"));
        Assert.assertEquals(COUNT_NEWS_ON_FRONTPAGE, spans.size());
        int startSize = getSizeOfHeadlinesTable();
        for (int i = 0; i < spans.size(); i++) {
            WebElement current = spans.get(i);
            WebElement a  = current.findElement(By.tagName("a"));
            String title = a.getText();
            String url = a.getAttribute("href");
            insertNewsItemToDatabase(title, url);
        }
        int endSize = getSizeOfHeadlinesTable();
        Assert.assertEquals(COUNT_NEWS_ON_FRONTPAGE, endSize-startSize);
    }

    private void insertNewsItemToDatabase(String title, String url) {
        String sql = "insert into headlines values(null, ?, ?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, title);
            ps.setString(2, url);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private int getSizeOfHeadlinesTable() {
        String sql = "select count(*) from headlines";
        int size = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            rs.next();
            size = rs.getInt("count(*)");
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return size;
    }

    @Test
    public void testCars() {
        driver.get("https://atlanta.craigslist.org/search/cta#search=1~gallery~0~1");
        //driver.manage().window().setSize(new Dimension(400, 600));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        List<WebElement> cards = driver.findElements(By.className("gallery-card"));
        Assert.assertEquals(120, cards.size());
        // class "label" for title
        // class "priceinfo" for price
        // class "main" for anchor to url
        for (int i = 0; i < cards.size(); i++) {
            WebElement current = cards.get(i);
            WebElement title = current.findElement(By.className("label"));
            String subject = title.getText();
            String priceString = "";
            try {
                WebElement priceinfo = current.findElement(By.className("priceinfo"));
                priceString = priceinfo.getText();
            } catch(NoSuchElementException e) {
                priceString = "-1";
            }
            WebElement a = current.findElement(By.className("main"));
            String url = a.getAttribute("href");
        }

    }

    @Test
    public void printDate() {
        System.out.println(new Date());

    }


}
