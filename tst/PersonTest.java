import assignment8.CacheManager;
import assignment8.DiskManager;
import assignment8.Person;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class PersonTest {

    private CacheManager cacheManager;
    private DiskManager diskManager;

    private Person DNE_PERSON;
    private int DNE_PHONE = 4444;
    private String DNE_FIRST_NAME = "Ghost";
    private String DNE_LAST_NAME = "Man";

    private Person CACHE_PERSON;
    private int CACHE_PHONE = 1234;
    private String CACHE_FIRST_NAME = "Johnny";
    private String CACHE_LAST_NAME = "Johnson";

    private Person DISK_PERSON;
    private int DISK_PHONE = 4321;
    private String DISK_FIRST_NAME = "Tommy";
    private String DISK_LAST_NAME = "Thompson";

    @Before
    public void setUp() {
        cacheManager = Mockito.mock(CacheManager.class);
        diskManager = Mockito.mock(DiskManager.class);
        DNE_PERSON = new Person(cacheManager, diskManager);
        CACHE_PERSON = new Person(cacheManager, diskManager);
        DISK_PERSON = new Person(cacheManager, diskManager);
    }

    @Test
    public void testPersonDNE() {
       DNE_PERSON.setPerson(DNE_PHONE, DNE_FIRST_NAME, DNE_LAST_NAME);

       Mockito.when(cacheManager.getPerson(DNE_PHONE)).thenReturn(null);
       Mockito.when(diskManager.getPerson(DNE_PHONE)).thenReturn(null);

       String actualFullName = DNE_PERSON.getFullName();

       Assert.assertEquals("", actualFullName);

       Mockito.verify(cacheManager).getPerson(DNE_PHONE);
       Mockito.verify(diskManager).getPerson(DNE_PHONE);
       Mockito.verifyNoMoreInteractions(cacheManager);
       Mockito.verifyNoMoreInteractions(diskManager);
    }

    @Test
    public void testPersonInCache() {
        CACHE_PERSON.setPerson(CACHE_PHONE, CACHE_FIRST_NAME, CACHE_LAST_NAME);

        Mockito.when(cacheManager.getPerson(CACHE_PHONE)).thenReturn(CACHE_PERSON);
        Mockito.when(diskManager.getPerson(CACHE_PHONE)).thenReturn(null);

        String actualFullName = CACHE_PERSON.getFullName();

        Assert.assertEquals("Johnny Johnson", actualFullName);

        Mockito.verify(cacheManager).getPerson(CACHE_PHONE);
        Mockito.verifyNoMoreInteractions(cacheManager);
        Mockito.verifyNoInteractions(diskManager);
    }

    @Test
    public void testPersonInDisk() {
        DISK_PERSON.setPerson(DISK_PHONE, DISK_FIRST_NAME, DISK_LAST_NAME);

        Mockito.when(cacheManager.getPerson(DISK_PHONE)).thenReturn(null);
        Mockito.when(diskManager.getPerson(DISK_PHONE)).thenReturn(DISK_PERSON);

        String actualFullName = DISK_PERSON.getFullName();

        Assert.assertEquals("Tommy Thompson", actualFullName);

        Person p = diskManager.getPerson(DISK_PHONE);
        Assert.assertTrue(p.equals(DISK_PERSON));

        Mockito.verify(cacheManager).getPerson(DISK_PHONE);
        Mockito.verify(diskManager, Mockito.times(2)).getPerson(DISK_PHONE);
        Mockito.verifyNoMoreInteractions(cacheManager);
        Mockito.verifyNoMoreInteractions(diskManager);

    }

}
