import com.company.Inventory;
import com.company.Store;
import com.company.Game;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class InventoryMockTests {

    @Test
    public void testCheapestGame() {
        Inventory invMock = Mockito.mock(Inventory.class);
        Game madden = new Game("12345","PC", "Madden NFL", "1/1/2024",
                "EA Sports", "Sports", 60.0, "awesome");
        Mockito.when(invMock.findCheapestGame()).thenReturn(madden);
        Assert.assertEquals("Madden NFL", invMock.findCheapestGame().getName());
    }

    @Test
    public void testMostExpensiveGame() {
        Inventory invMock = Mockito.mock(Inventory.class);
        Game mario = new Game("54321", "Nintendo", "Super Mario Bros", "1/1/1984", "Nintendo",
                "Platformer", 800, "great!");
        Mockito.when(invMock.findMostExpensiveGame()).thenReturn(mario);
        Assert.assertEquals("Super Mario Bros", invMock.findMostExpensiveGame().getName());
    }



}
