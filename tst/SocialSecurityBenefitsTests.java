import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

@RunWith(JUnitParamsRunner.class)
public class SocialSecurityBenefitsTests {
    private static WebDriver driver;
    private static final String SS_URL = " https://www.ssa.gov/OACT/quickcalc/";

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
    }

    @Test
    @Parameters({"1,1,1990,55000,2375","1,1,1980,65000,3043","1,1,1970,75000,3493"})
    public void testBenefits(int month, int day, int year, int salary,double expectedBenefit) {
        driver.get(SS_URL);
        WebElement dobmon = driver.findElement(By.name("dobmon"));
        WebElement dobday = driver.findElement(By.name("dobday"));
        WebElement yob = driver.findElement(By.name("yob"));
        WebElement earnings = driver.findElement(By.name("earnings"));
        dobmon.clear();
        dobday.clear();
        yob.clear();
        earnings.clear();
        dobmon.sendKeys(String.valueOf(month));
        dobday.sendKeys(String.valueOf(day));
        yob.sendKeys(String.valueOf(year));
        earnings.sendKeys(String.valueOf(salary));
        WebElement submit = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submit.click();
        WebElement submit2 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[3]/td/form/input[10]"));
        submit2.click();
        int yearOfBirth = year;
        for (int i = yearOfBirth + 17; i <= 2024; i++) {
            WebElement yearElement = driver.findElement(By.name(String.valueOf(i)));
            yearElement.clear();
            yearElement.sendKeys(String.valueOf(salary));
        }
        WebElement submit3 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[1]/td[1]/form/input[6]"));
        submit3.click();

        WebElement estFRA = driver.findElement(By.id("est_fra"));
        System.out.println(estFRA.getText());
        Assert.assertEquals(expectedBenefit, Double.parseDouble(estFRA.getText().replace("$","").replace(",","")), 0);
    }

    @Test
    @Parameters({"1,1,1960,2000000,5000","1,1,1960,1000000,5000","1,1,1960,168600,5000"})
    public void testMaxBenefit(int month, int day, int year, int salary,double expectedBenefit) {
        driver.get(SS_URL);
        WebElement dobmon = driver.findElement(By.name("dobmon"));
        WebElement dobday = driver.findElement(By.name("dobday"));
        WebElement yob = driver.findElement(By.name("yob"));
        WebElement earnings = driver.findElement(By.name("earnings"));
        dobmon.clear();
        dobday.clear();
        yob.clear();
        earnings.clear();
        dobmon.sendKeys(String.valueOf(month));
        dobday.sendKeys(String.valueOf(day));
        yob.sendKeys(String.valueOf(year));
        earnings.sendKeys(String.valueOf(salary));
        WebElement submit = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submit.click();
        WebElement submit2 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[3]/td/form/input[10]"));
        submit2.click();
        int yearOfBirth = year;
        for (int i = yearOfBirth + 17; i <= 2024; i++) {
            WebElement yearElement = driver.findElement(By.name(String.valueOf(i)));
            yearElement.clear();
            yearElement.sendKeys(String.valueOf(salary));
        }
        WebElement submit3 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[1]/td[1]/form/input[6]"));
        submit3.click();

        WebElement estFRA = driver.findElement(By.id("est_fra"));
        System.out.println(estFRA.getText());
        //Assert.assertEquals(expectedBenefit, Double.parseDouble(estFRA.getText().replace("$","").repl
    }

    @Test
    @Parameters({"2025,2025","2010,2014","2010,2019"})
    public void testMissingTenYears(int gapYearStart, int gapYearEnd) {
        // let's assume a person earning $55000 who was born in the year 1988
        // no rest = $2,433.00
        // 5 year rest = $2,339.00
        // 10 year rest = $2,284.00
        // 150 x 12 x 42 = around $75000
        driver.get(SS_URL);
        WebElement dobmon = driver.findElement(By.name("dobmon"));
        WebElement dobday = driver.findElement(By.name("dobday"));
        WebElement yob = driver.findElement(By.name("yob"));
        WebElement earnings = driver.findElement(By.name("earnings"));
        dobmon.clear();
        dobday.clear();
        yob.clear();
        earnings.clear();
        dobmon.sendKeys(String.valueOf(1));
        dobday.sendKeys(String.valueOf(1));
        yob.sendKeys(String.valueOf(1988));
        earnings.sendKeys(String.valueOf(55000));
        WebElement submit = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submit.click();
        WebElement submit2 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[3]/td/form/input[10]"));
        submit2.click();
        int yearOfBirth = 1988;
        for (int i = yearOfBirth + 17; i <= 2024; i++) {
            WebElement yearElement = driver.findElement(By.name(String.valueOf(i)));
            yearElement.clear();
            if (i >= gapYearStart && i <= gapYearEnd) {
                yearElement.sendKeys(String.valueOf(0));
            } else {
                yearElement.sendKeys(String.valueOf(55000));
            }
        }
        WebElement submit3 = driver.findElement(By.xpath("/html/body/table[3]/tbody/tr[1]/td[1]/form/input[6]"));
        submit3.click();
        WebElement estFRA = driver.findElement(By.id("est_fra"));
        System.out.println(estFRA.getText());
    }


}
