import assignment9.InvalidOrderException;
import assignment9.Order;
import assignment9.OrderManager;
import assignment9.ProcessOrders;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class OrderManagerTest {

    private OrderManager orderManager;
    private ProcessOrders processorMock;


    @Before
    public void setup() {

        // create a ProcessOrders mock object
        processorMock = Mockito.mock(ProcessOrders.class);

        // create orderManager instance with mock object
        orderManager = new OrderManager(processorMock);
    }

    @Test
    public void testProcessValidOrder() {
        //***
        //GIVEN - setup mock and other necessary objects
        //***
        boolean result = orderManager.processOrder(Mockito.anyInt());

        //***
        //WHEN - have the mock object do something (that you want to test)
        //***
        Assert.assertTrue(result);
        //***
        //THEN - verify expected result
        //***
        Mockito.verify(processorMock).shipOrder(Mockito.anyInt());
        Mockito.verifyNoMoreInteractions(processorMock);

    }

    @Test
    public void testProcessInvalidOrder() {
        //***
        //GIVEN - setup mock and define expected behavior
        //***
        Mockito.doThrow(new InvalidOrderException()).when(processorMock).shipOrder(1984);

        //***
        //WHEN - have the mock object do something (that you want to test)
        //***
        boolean result = orderManager.processOrder(1984);

        //***
        //THEN - verify expected result
        //***
        Assert.assertFalse(result);
        Mockito.verify(processorMock).shipOrder(1984);
        Mockito.verifyNoMoreInteractions(processorMock);
    }


    @Test
    public void testProfitHappyCase() {
        //***
        //GIVEN - setup mock and define expected behavior
        //***
        orderManager.processOrder(1);
        orderManager.processOrder(2);
        orderManager.processOrder(3);


        //***
        //WHEN - have the mock object do something (that you want to test)
        //***
        double actualProfit = orderManager.calculateProfit();
        double expectedProfit = 3 * 1.11;

        Assert.assertEquals(expectedProfit, actualProfit, 0);

        //***
        //THEN - verify expected result
        //***
        Mockito.verify(processorMock).shipOrder(1);
        Mockito.verify(processorMock).shipOrder(2);
        Mockito.verify(processorMock).shipOrder(3);
        Mockito.verify(processorMock, Mockito.times(2)).getFraudulentOrders();
        Mockito.verifyNoMoreInteractions(processorMock);

    }


    @Test
    public void testProfitWithFrauds() {
        //***
        //GIVEN - setup mock and define expected behavior
        //***
        List<Order> fraudulentOrders = new ArrayList<>();
        fraudulentOrders.add(new Order(4));
        fraudulentOrders.add(new Order(13));
        fraudulentOrders.add(new Order(1984));
        Mockito.when(processorMock.getFraudulentOrders()).thenReturn(fraudulentOrders);

        orderManager.processOrder(1);
        orderManager.processOrder(2);
        orderManager.processOrder(3);
        orderManager.processOrder(4);
        orderManager.processOrder(13);
        orderManager.processOrder(1984);

        //***
        //WHEN - have the mock object do something (that you want to test)
        //***
        double actualProfit = orderManager.calculateProfit();
        double expectedProfit = 3*1.11 - 3*1.33;
        Assert.assertEquals(expectedProfit, actualProfit, 0);

        //***
        //THEN - verify expected result
        //***
        Mockito.verify(processorMock).shipOrder(1);
        Mockito.verify(processorMock).shipOrder(2);
        Mockito.verify(processorMock).shipOrder(3);
        Mockito.verify(processorMock).shipOrder(4);
        Mockito.verify(processorMock).shipOrder(13);
        Mockito.verify(processorMock).shipOrder(1984);
        Mockito.verify(processorMock,Mockito.times(2)).getFraudulentOrders();
        Mockito.verifyNoMoreInteractions(processorMock);

    }

}
