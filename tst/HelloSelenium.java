import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class HelloSelenium {

    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        //you need the absolute path to the driver!
        //System.setProperty("webdriver.chrome.driver","/Users/tim/Downloads/ITEC4260Spring2024Section02/chromedriver");
        //driver = new ChromeDriver();
        System.setProperty("webdriver.gecko.driver","/Users/tim/Downloads/ITEC4260Spring2024Section02/geckodriver");
        driver = new FirefoxDriver();
        //System.setProperty("webdriver.edge.driver","/Users/tim/Downloads/ITEC4260Spring2024Section02/msedgedriver");
        //driver = new EdgeDriver();
    }

    @Test
    public void testGoogle() {
        driver.get("https://www.google.com");
        WebElement searchBox = driver.findElement(By.name("q"));
        searchBox.sendKeys("GGC");
        searchBox.submit();
        //WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        //wait.until(ExpectedConditions.titleContains("GGC"));
        Assert.assertTrue(driver.getTitle().contains("GGC"));
    }

    @Test
    public void testIHG() {
        driver.get("https://ihgrewardsdineandearn.rewardsnetwork.com/join");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.titleContains("IHG"));
        WebElement firstName = driver.findElement(By.cssSelector("#firstName"));
        firstName.sendKeys("Anna");
        WebElement lastName = driver.findElement(By.cssSelector("#lastName"));
        lastName.sendKeys("Smith");
        WebElement zipCode = driver.findElement(By.cssSelector("#zipCode"));
        zipCode.sendKeys("30043");
        WebElement partnerProgramNumber = driver.findElement(By.cssSelector("#partnerProgramNumber"));
        partnerProgramNumber.sendKeys("123456789");
        WebElement tos =driver.findElement(By.cssSelector("#acceptTOS"));
        tos.click();
        WebElement joinButton = driver.findElement(By.cssSelector(".sc-pGaPU"));
        joinButton.submit();

    }
}
