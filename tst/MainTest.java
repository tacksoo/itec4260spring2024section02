import com.company.Game;
import com.company.Main;
import com.company.SteamAppStore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.StringReader;
import java.net.URL;

public class MainTest {

    @Test
    public void testAddition() {
        // expected , actual
        Assert.assertEquals(4, Main.add(2,2));
    }

    @Test
    public void testGame() throws Exception {
        //Game g2 = new Game("PC", "Cyberpunk 2077","12/10/2020","CD Projekt Red Studio","Action RPG", 5999, "M");
        // Steam ID of games recommended from 4260 sections
        String [] favGames = { "1966720", "582010", "1086940" };
        for (int i = 0; i < favGames.length; i++) {
            Game g = SteamAppStore.getGameFromSteam(favGames[i]);
            g.printGame();
            System.out.println();
        }
    }

    @Test(expected = ArithmeticException.class)
    public void testDivideByZero() {
        int x = 1/0;
    }

    @Test
    public void testDoubles() {
        double d = 100/3.0;
        Assert.assertEquals(33, d, 0.5);
    }

    @Test
    public void testDownload() throws Exception {
        URL url = new URL("https://gist.githubusercontent.com/tacksoo/349702bd06852814fba06a4df48e32d8/raw/ead974003d382b1b6d5de112ebfa1d1b66abceb6/myinventory.csv");
        String data = IOUtils.toString(url,"UTF-8");
        //System.out.println(data);
        CSVParser parser = CSVParser.parse(new StringReader(data), CSVFormat.DEFAULT.builder().build());
        for(CSVRecord record: parser) {
            System.out.println(record.get(3));
        }
    }





}
