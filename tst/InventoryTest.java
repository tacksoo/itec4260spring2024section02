import com.company.Game;
import com.company.Inventory;
import com.company.SteamAppStore;
import org.junit.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class InventoryTest {

    private Inventory inventory;

    @Before
    public void setUp() {
        inventory = new Inventory();
        String [] favGames = { "1966720", "582010", "1086940"};
        for (int i = 0; i < favGames.length; i++) {
            Game g = SteamAppStore.getGameFromSteam(favGames[i]);
            g.printGame();
            //System.out.println(g.getName() + " " + g.getRecommendationCount());
            inventory.add(g);
        }
    }

    @Test
    public void testAddInventory() {
        Assert.assertEquals(3, inventory.getSize());
        Game g = SteamAppStore.getGameFromSteam("427520");
        inventory.add(g);
        Assert.assertEquals(4, inventory.getSize());
        inventory.add(g);
        // overwrite duplicate game
        Assert.assertEquals(4, inventory.getSize());
        Game g2 = SteamAppStore.getGameFromSteam("270880");
        inventory.add(g2);
        Assert.assertEquals(5, inventory.getSize());
    }

    //@Ignore
    @Test
    public void testRemoveGame() {
        Game g = SteamAppStore.getGameFromSteam("1966720");
        inventory.remove(g);
        Assert.assertEquals(2, inventory.getSize());
        inventory.remove(g);
        Assert.assertEquals(2, inventory.getSize());
        Game g2 = SteamAppStore.getGameFromSteam("582010");
        inventory.remove(g2);
        Assert.assertEquals(1, inventory.getSize());
        inventory.remove(g2);
        Assert.assertEquals(1, inventory.getSize());
    }

    @Test
    public void testPrintAveragePrint() {
        double total = 999 + 2999 + 5999;
        double expectedAverage = total/3.0;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        System.setOut(ps);
        inventory.printAveragePriceOfAllGames();
        String output = baos.toString();
        double actual = Double.parseDouble(output);
        Assert.assertEquals(expectedAverage, actual, 0.5);
    }

    @Test
    public void testGetCheapestGame() {
        Assert.assertEquals("Lethal Company", inventory.findCheapestGame().getName());
        Inventory myInventory = new Inventory();
        Assert.assertNull("check empty inventory returns null", myInventory.findCheapestGame());
    }

    @Test
    public void testGetMostHighlyRate() {
        Assert.assertEquals("Baldur's Gate 3", inventory.findMostHighlyRatedGame().getName());
        Inventory myInventory = new Inventory();
        Assert.assertNull(myInventory.findMostHighlyRatedGame());
    }

}
