import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(JUnitParamsRunner.class)
public class ParameterExample {

    @Test
    @Parameters({"sally","sam","sammy"})
    public void testName(String str) {
        Assert.assertTrue(str.length() > 0);
    }

    @Test
    @Parameters({"sally,2004","sam,1999","sammy,2010"})
    public void testNameAndYOB(String name, int year) {
        Assert.assertTrue(name.length() > 0);
        Assert.assertTrue(year > 1998);
    }


}
