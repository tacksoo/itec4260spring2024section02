import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.File;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class MidtermTest {

    public static final String DEAL_URL = "https://www.cheapshark.com/api/1.0/deals?storeID=1&upperPrice=15";
    public static final String SCHEDULE_URL = "https://gist.githubusercontent.com/tacksoo/d1fcb51f8921cdc90d1ffadb0b63b768/raw/6c9a8b9ffadd87b4bd0217b91cdd90bb9e227ef2/schedule.csv";
    public static final String FINANCE_URL = "https://gist.githubusercontent.com/tacksoo/b9edbfc8c03e1ca89d459bf1af39842d/raw/75abf553a0297d9202b1a568f185f735055d6f81/stonks.csv";
    public static final String HACKER_NEWS = "https://news.ycombinator.com";
    public static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
    }

    @Test
    public void testHackernews() throws Exception {
        driver.get(HACKER_NEWS);
        List<WebElement> headlines = driver.findElements(By.className("rank"));
        Assert.assertEquals(30, headlines.size());
    }


    @Test
    public void testGameDeals() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(new URL(DEAL_URL));
        int count = 0;
        for (int i = 0; i < root.size(); i++) {
            String rating  = root.get(i).get("steamRatingText").asText();
            if (rating.equals("Very Positive")) {
                count += 1;
            }
        }
        System.out.println(count);
        Assert.assertTrue(count > 0);
    }

    @Test
    public void testLinesInFile() throws Exception {
        String text = IOUtils.toString(new URL(SCHEDULE_URL), "UTF-8");
        String [] lines = text.split("\n");
        Assert.assertEquals(29, lines.length);
    }

    @Test
    public void testFinance() throws Exception {
        String text = IOUtils.toString(new URL(FINANCE_URL), "UTF-8");
        CSVParser parser = new CSVParser(new StringReader(text), CSVFormat.DEFAULT.builder().setHeader().build());
        double min = Double.MAX_VALUE;
        for(CSVRecord record: parser) {
            String col3 = record.get(" Prediction");
            double percentage = Double.parseDouble(col3.replace("%",""));
            min = Math.min(min, percentage);
        }
        FileUtils.writeStringToFile(new File("stonks.csv"), String.valueOf(min), "UTF-8");
        Assert.assertTrue(Files.exists(Path.of("stonks.csv")));
    }



}
