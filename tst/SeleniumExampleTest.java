import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SeleniumExampleTest {

    private static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        System.setProperty("webdriver.gecko.driver","geckodriver");
        //System.setProperty("webdriver.chrome.driver","chromedriver");

        driver = new FirefoxDriver();
        //ChromeOptions options = new ChromeOptions();
        //options.addArguments("--remote-allow-origins=*");
        //driver = new ChromeDriver(options);
    }


    @Test
    public void googleSearchExample() {
        driver.get("https://www.google.com");
        WebElement element = driver.findElement(By.name("q"));
        element.clear();
        element.sendKeys("GGC");
        element.submit();

        //driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(10));
        wait.until(ExpectedConditions.titleContains("GGC"));
        String url = driver.getCurrentUrl();
        String title = driver.getTitle();
        System.out.println(url);
        System.out.println(title);
        Assert.assertTrue("Search title should contain GGC", title.contains("GGC"));
    }

    @AfterClass
    public static void cleanUp() {
        driver.close();
    }


    @Test
    public void testCraigsListBestOf() {
        driver.get("https://www.craigslist.org/about/best/all/");
        WebElement element  =  driver.findElement(By.cssSelector(".bestoftoc > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)"));
        System.out.println(element.getText());
    }


    @Ignore
    @Test
    public void testGGC() {
        driver.get("https://www.ggc.edu");
    }

}
